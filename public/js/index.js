(function($){
	$.ajax({
        url: "https://jsonplaceholder.typicode.com/posts",
        data: {},
        type: 'GET',
        contentType: false,
        processData: false,
        cache: false,
        success: function(data) {
            // console.log(data);
            var html = '';
            for(var i = 0; i < data.length; i++){
            	var post = data[i];
            	var show = '';
            	if(i == 0){
            		show = ' show';
            	}
            	html =html + '<li class="list-group-item d-flex justify-content-between align-items-start">'
            		 + '	<a href="#" postId="'+post.id+'" title="'+post.title+'" body="'+post.body+'" class="item_post" data-bs-toggle="modal" data-bs-target="#modalComentarios">'
            		 + '		<div class="ms-2 me-auto">'
            		 + '			<div class="fw-bold">'+post.title+'</div>'
            		 + '			'+post.body
            		 + '		</div>'
            		 + '	</a>'
            		 + '</li>';
            }
            $('#content_posts').html(html);
        },
        error: function(xhr, status) {
            console.log(xhr);
        },
        complete: function(xhr, status) {
            console.log(status);
        },
        xhr: function() {
            var xhr = $.ajaxSettings.xhr();
            xhr.onprogress = function(evt) {
                // var porcentaje = Math.floor((evt.loaded / evt.total * 100));
                // console.log(porcentaje);
            };
            return xhr;
        }
    });

    $(document).on('click', '.item_post', function(event) {
    	var postId = $(this).attr('postId');
    	var title = $(this).attr('title');
    	var body = $(this).attr('body');
    	$.ajax({
	        url: "https://jsonplaceholder.typicode.com/posts/"+postId+"/comments",
	        data: {},
	        type: 'GET',
	        contentType: false,
	        processData: false,
	        cache: false,
	        success: function(data) {
	            console.log(data);
	            $('#head_post').text(title);
	            $('#body_post').text(body);
	            var html = '';
	            for(var i = 0; i < data.length; i++){
	            	var comment = data[i];
	            	html = html + '<li class="list-group-item"><b>'+comment.name+'</b><br><b>'+comment.email+'</b><br><p>'+comment.body+'</p></li>';
	            }
	            $('#content_commentarios').html(html);
	        },
	        error: function(xhr, status) {
	            console.log(xhr);
	        },
	        complete: function(xhr, status) {
	            console.log(status);
	        },
	        xhr: function() {
	            var xhr = $.ajaxSettings.xhr();
	            xhr.onprogress = function(evt) {
	                // var porcentaje = Math.floor((evt.loaded / evt.total * 100));
	                // console.log(porcentaje);
	            };
	            return xhr;
	        }
	    });
    });
})(jQuery);